// mongodb+srv://LaFenice:_3Valentine-4@cluster0.znk4w.mongodb.net/S31?retryWrites=true&w=majority

let express = require('express');
let app = express();
let mongoose = require ('mongoose');
const PORT = 5000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let tasksRoutes = require('./routes/tasksRoutes.js')


mongoose.connect("mongodb+srv://LaFenice:_3Valentine-4@cluster0.znk4w.mongodb.net/S31?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> console.log(`connected to database`)).catch((error)=>console.log(error));

app.use("/", tasksRoutes);



app.listen(PORT, () =>(console.log(`server running on port ${PORT}`)));