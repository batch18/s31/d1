const User = require('./../models/User');

//insert a new user
module.exports.createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (error, savedUser) => {
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}


module.exports.getAll = () => {
	return User.find().then( (error, result) => {
		if(error){
			return error
		}
		else {
			return result
		}
	})
}

module.exports.getUser = (params) => {
	return User.findById(params).then( (error, result) => {
		if(error){
			return error
		}
		else {
			return result
		}
	})
}

module.exports.updateUser = (params, reqBody) => {
	return User.findByIdAndUpdate(params, reqBody, {new:true}).then( (error, result) => {
		if(error){
			return error
		}
		else {
			return result
		}
	})
}


module.exports.deleteUser = (params) => {
	return User.findByIdAndDelete(params).then( (error, result) => {
		if(error){
			return error
		}
		else {
			return result
		}
	})
}