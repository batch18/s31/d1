let express = require('express');
let router = express.Router();
let User = require('./../models/User');

let userController = require('./../controllers/userControllers.js');

router.post('/add-task', (req, res)=>{
	/*let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	});
	newUser.save((error, registeredUser)=>{
		if (error) {
			console.log(error);
		}
		else {
			res.send(`Successfully registered: ${registeredUser}`);
		}

	})*/
	userController.createUser(req.body).then(result => res.send(result))
});

router.get('/users', (req, res) => {
	/*User.find({}, (error, userRecords)=> {
		if(error){
			res.send(error)
		}
		else {
			res.send(`users: ${userRecords}`)
		}
	})*/
	userController.getAll().then(result => res.send(result))
});

router.get('/users/:id', (req,res) => {
	params = req.params.id/*
	User.findById(params, (error, result)=> {
		if(error){
			res.send(error)
		}
		else {
			res.send(`User: ${result}`)
		}
	})*/
	userController.getUser(params).then(result => res.send(result))
});


router.put('/users/:id', (req, res) => {
	let params = req.params.id
	let reqBody = req.body
	/*User.findByIdAndUpdate(params, req.body, {new:true}, (error, result) =>{
		if (error){
			res.send(error)
		}
		else {
			res.send(result)
		}
	})*/
	userController.updateUser(params, reqBody).then(result => res.send(result))
});

router.delete('/delete-user/:id', (req, res)=> {
	let params = req.params.id
	/*User.findByIdAndDelete(params, (error, deletedUser) => {
		if(error){
			res.send(error)
		}
		else {
			res.send(`User ${deletedUser} deleted Successfully`)
		}
	});*/
	userController.deleteUser(params).then(result => res.send(result))
});



module.exports = router;